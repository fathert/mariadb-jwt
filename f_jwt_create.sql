/* 
 * Copyright (C) 2018 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use TEST;;

/* Create a JSON Web Token for the user id specificed. */
create or replace function f_jwt_create(
  secret_key varchar(1024),
  user varchar(64)
)

returns varchar(2048)

begin

  declare header varchar(256);
  declare payload varchar(1024);
  declare jwt varchar(2048);
  declare unixNow integer;
  declare unixExp integer;
  
  set unixNow = timestampdiff(second, '1970-01-01 00:00:00', current_timestamp);
  set unixExp = unixNow + 8 * 3600;
  
  set header = json_object("alg", "HS256", "typ", "JWT");
  set payload = json_object("sub", user, "iat", cast(unixNow as integer), "exp", cast(unixExp as integer));

  set jwt = concat(to_base64(header), '.', to_base64(payload));
  set jwt = replace(replace(replace(replace(jwt, '/', '_'), '+', '-'), '=', ''), '\n', '');
  set jwt = concat(jwt, '.', to_base64(unhex(f_hmac_sha256(secret_key, jwt))));
  
  /* Convert to URL-compliant base-36. */
  set jwt = replace(replace(replace(replace(jwt, '/', '_'), '+', '-'), '=', ''), '\n', '');
  	    
  return jwt;

end
;;

/* Tests. */
set @secret = 'StrangeLove';;
set @user = 'dave.gahan@depechemode.com';;

select f_jwt_create(@secret, @user);;
