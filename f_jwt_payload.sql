/* 
 * Copyright (C) 2018 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use TEST;;

/* Validate a JSON Web Token and return the payload. */
create or replace function f_jwt_payload(
  secret_key varchar(1024),
  jwt varchar(2048)
)

returns varchar(1024)

begin

  declare header varchar(256);
  declare payload varchar(512);
  declare signature varchar(128);
  

  /* Convert from URL-compliant base-36. */
  set jwt = replace(replace(jwt, '_', '/'), '-', '+');
  
  set header = substring_index(jwt, '.', 1);
  set header = concat(header, left('====', (cast(length(header) / 4 as integer)) * 4 - length(header)));
  set header = from_base64(header);
  
  set payload = substring_index(substring_index(jwt, '.', 2), '.', -1);
  set payload = concat(payload, left('====', (cast(length(payload) / 4 as integer)) * 4 - length(payload)));
  set payload = from_base64(payload);

  set signature = substring_index(jwt, '.', -1);
  set signature = concat(signature, left('====', (cast(length(signature) / 4 as integer)) * 4 - length(signature)));
  
  if unhex(f_hmac_sha256(secret_key, substring_index(jwt, '.', 2))) <> from_base64(signature) then
    signal SQLSTATE '38000'
      set message_text = 'Signature invalid';
  end if;
       
  return payload;

end
;;

/* Tests. */
set @secret = 'StrangeLove';;
set @jwt = 'eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCJ9.eyJzdWIiOiAiZGF2ZS5nYWhhbkBkZXBlY2hlbW9kZS5jb20iLCAiaWF0IjogMTUzMzIyMDE2MywgImV4cCI6IDE1MzMyNDg5NjN9.uJV01OAFeirlLx18q-2xhknzskn1muRA30-pAK9UBeU';;

/* Extract subject from payload. */
select json_extract(f_jwt_payload(@secret, @jwt), "$.sub");;

