/* 
 * Copyright (C) 2018 Tim Fathers (tim@fathers.me.uk)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use TEST;;

/* Create an HMAC-SHA256 hash. */
create or replace function f_hmac_sha256(
  secret_key varchar(1024),
  message varchar(16384)
)

returns char(128)

begin

  declare hexkey char(128);
  declare o_key_pad, i_key_pad binary(64);
  declare hmac char(128);
  
  set hexkey = rpad(
                 case 
                   when length(secret_key) > 64 then SHA2(secret_key, 256)
                   else hex(secret_key)
                 end, 128, '0'
               );

  set o_key_pad = unhex(concat(
    hex(conv(substring(hexkey,   1, 16), 16, 10) ^ conv(rpad('', 16, '5c'), 16, 10)),
    hex(conv(substring(hexkey,  17, 16), 16, 10) ^ conv(rpad('', 16, '5c'), 16, 10)),
    hex(conv(substring(hexkey,  33, 16), 16, 10) ^ conv(rpad('', 16, '5c'), 16, 10)),
    hex(conv(substring(hexkey,  49, 16), 16, 10) ^ conv(rpad('', 16, '5c'), 16, 10)),
    hex(conv(substring(hexkey,  65, 16), 16, 10) ^ conv(rpad('', 16, '5c'), 16, 10)),
    hex(conv(substring(hexkey,  81, 16), 16, 10) ^ conv(rpad('', 16, '5c'), 16, 10)),
    hex(conv(substring(hexkey,  97, 16), 16, 10) ^ conv(rpad('', 16, '5c'), 16, 10)),
    hex(conv(substring(hexkey, 113, 16), 16, 10) ^ conv(rpad('', 16, '5c'), 16, 10))
  ));

  set i_key_pad = unhex(concat(
    hex(conv(substring(hexkey,   1, 16), 16, 10) ^ conv(rpad('', 16, '36'), 16, 10)),
    hex(conv(substring(hexkey,  17, 16), 16, 10) ^ conv(rpad('', 16, '36'), 16, 10)),
    hex(conv(substring(hexkey,  33, 16), 16, 10) ^ conv(rpad('', 16, '36'), 16, 10)),
    hex(conv(substring(hexkey,  49, 16), 16, 10) ^ conv(rpad('', 16, '36'), 16, 10)),
    hex(conv(substring(hexkey,  65, 16), 16, 10) ^ conv(rpad('', 16, '36'), 16, 10)),
    hex(conv(substring(hexkey,  81, 16), 16, 10) ^ conv(rpad('', 16, '36'), 16, 10)),
    hex(conv(substring(hexkey,  97, 16), 16, 10) ^ conv(rpad('', 16, '36'), 16, 10)),
    hex(conv(substring(hexkey, 113, 16), 16, 10) ^ conv(rpad('', 16, '36'), 16, 10))
  ));

  set hmac = sha2(concat(o_key_pad, unhex(sha2(concat(i_key_pad, message), 256))), 256);
  
  return hmac;
end
;;

/* Tests. */
set @secret = 'secret-key2';;
set @message = 'Top secret message!';;

select @message, f_hmac_sha256(@secret, @message);;
